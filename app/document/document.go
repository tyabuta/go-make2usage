package document

import (
	"bufio"
	"gitlab.com/tyabuta/go-make2usage/app/util"
	"os"
	"regexp"
	"strings"
)

type Kind int

const (
	KindCommand  Kind = 0
	KindVariable Kind = 1
)

type Hint struct {
	Type     Kind
	Name     string
	Comments []string
}

var (
	commandRegex  = regexp.MustCompile(`^(\w[\w-]*):.*$`)
	variableRegex = regexp.MustCompile(`^(\w+)\s*(=|\?=|:=|::=|\+=|!=).*$`)
)

type Document struct {
	Hints []Hint
}

func New(fileNames ...string) (*Document, error) {
	doc := &Document{}
	for _, fileName := range fileNames {
		hints, err := readHints(fileName)
		if err != nil {
			return nil, err
		}
		doc.Hints = append(doc.Hints, hints...)
	}
	return doc, nil
}

func (d *Document) FindHints(hintType Kind, isAll bool) []Hint {
	arr := make([]Hint, 0, len(d.Hints))
	for _, hint := range d.Hints {
		if hintType != hint.Type {
			continue
		}
		if !isAll && 0 == len(hint.Comments) {
			continue
		}
		arr = append(arr, hint)
	}
	return arr
}

func readHints(fileName string) ([]Hint, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer util.SafeClose(f)

	var hints []Hint
	var buf []string
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		line := sc.Text()

		if strings.HasPrefix(line, "## ") {
			buf = append(buf, line[3:])
			continue
		}

		if mc := variableRegex.FindStringSubmatch(line); 0 < len(mc) {
			hints = append(hints, Hint{Type: KindVariable, Name: mc[1], Comments: buf})
			buf = []string{}
			continue
		}

		if mc := commandRegex.FindStringSubmatch(line); 0 < len(mc) {
			hints = append(hints, Hint{Type: KindCommand, Name: mc[1], Comments: buf})
			buf = []string{}
			continue
		}

		if 0 < len(buf) {
			buf = []string{}
		}
	}

	return hints, nil
}
