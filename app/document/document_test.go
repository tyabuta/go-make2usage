package document

import (
	ast "github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func TestNew(t *testing.T) {
	assert := ast.New(t)

	doc, err := New("testdata/count1.mk")
	assert.Equal(nil, err)

	for _, v := range doc.Hints {
		log.Println(v)
	}

	assert.Equal(6, len(doc.Hints))
}

func TestFindHints(t *testing.T) {
	assert := ast.New(t)

	doc, err := New("testdata/find1.mk")
	assert.Equal(nil, err)

	patterns := []struct {
		extract int
		kind    Kind
		isAll   bool
	}{
		{2, KindCommand, false},
		{3, KindCommand, true},
		{1, KindVariable, false},
		{3, KindVariable, true},
	}

	for _, p := range patterns {
		hints := doc.FindHints(p.kind, p.isAll)
		assert.Equal(p.extract, len(hints))
	}
}
