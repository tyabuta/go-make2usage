package app

import (
	"fmt"
	"gitlab.com/tyabuta/go-make2usage/app/document"
	"gitlab.com/tyabuta/go-make2usage/app/util"
)

type Option struct {
	ShowAllVariable bool
	ShowAllCommand  bool
}

func OutputUsage(doc *document.Document, option Option) {
	indentSpaceCount := 0
	for _, info := range doc.Hints {
		count := len(info.Name)
		if count > indentSpaceCount {
			indentSpaceCount = count
		}
	}
	indentSpace := util.Space(4 + indentSpaceCount + 1)

	fmt.Println("USAGE")
	fmt.Println("    make <commands> [variables]")
	fmt.Println("")

	variableHints := doc.FindHints(document.KindVariable, option.ShowAllVariable)
	commandHints := doc.FindHints(document.KindCommand, option.ShowAllCommand)

	if 0 < len(variableHints) {
		fmt.Println("VARIABLES")
		for _, info := range variableHints {
			outputHint(info, indentSpace)
		}
		fmt.Println("")
	}

	if 0 < len(commandHints) {
		fmt.Println("COMMANDS")
		for _, info := range commandHints {
			outputHint(info, indentSpace)
		}
		fmt.Println("")
	}
}

func outputHint(hint document.Hint, indentSpace string) {
	if 0 == len(hint.Comments) {
		fmt.Printf("    %s\n\n", hint.Name)
		return
	}

	for i, commentLine := range hint.Comments {
		if 0 == i {
			name := fmt.Sprintf("    %s", hint.Name)
			s := util.Space(len(indentSpace) - len(name))
			fmt.Printf("%s%s -- %s\n", name, s, commentLine)
			continue
		}

		fmt.Printf("%s    %s\n", indentSpace, commentLine)
	}
}
