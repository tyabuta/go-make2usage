package util

import (
	ast "github.com/stretchr/testify/assert"
	"testing"
)

func TestSpace(t *testing.T) {
	assert := ast.New(t)

	assert.Equal("  ", Space(2))
	assert.Equal("    ", Space(4))
}
