package util

import (
	"io"
	"log"
)

func SafeClose(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.Println(err)
	}
}

func Space(count int) string {
	buf := make([]byte, 0, count)
	for i := 0; i < count; i++ {
		buf = append(buf, ' ')
	}
	return string(buf)
}
