#!/usr/bin/env bash

repo=tyabuta/go-make2usage
binName=make2usage

if [ -f "$binName" ]; then
    exit 0
fi

if [ "$(uname)" == 'Linux' ]; then
    pathToBin=bin/linux64/$binName
elif [ "$(uname)" == 'Darwin' ]; then
    pathToBin=bin/darwin64/$binName
else
    pathToBin=bin/windows64/$binName
fi

url=https://gitlab.com/$repo/-/jobs/artifacts/master/raw/$pathToBin?job=compile
echo "Downloading $url"
curl -L -o $binName $url
chmod a+x $binName

echo "Installed $binName"
