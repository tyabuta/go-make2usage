package main

import (
	"fmt"
	"github.com/urfave/cli"
	"gitlab.com/tyabuta/go-make2usage/app"
	"gitlab.com/tyabuta/go-make2usage/app/document"
	"log"
	"os"
)

var (
	version   = "xxxxxxxxx"
	revision  = "xxxxxxxxx"
	builtAt   = "xxxxxxxxx"
	goVersion = "xxxxxxxxx"
)

var (
	option app.Option
)

func main() {
	App := cli.NewApp()
	App.Version = version
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("version %s (rev:%s)\n", c.App.Version, revision)
		fmt.Printf("built by %s at %s\n", goVersion, builtAt)
	}
	App.Name = "make2usage"
	App.Usage = "Generate usage from Makefiles."
	App.ArgsUsage = "<Makefile, ...>"
	App.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "all-variable, V",
			Destination: &option.ShowAllVariable,
			Usage:       "show all variable"},
		cli.BoolFlag{
			Name:        "all-command, C",
			Destination: &option.ShowAllCommand,
			Usage:       "show all command"},
	}
	App.Action = execute

	err := App.Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}

func execute(c *cli.Context) error {
	if 1 > c.NArg() {
		cli.ShowAppHelpAndExit(c, 1)
	}

	doc, err := document.New(c.Args()...)
	if err != nil {
		return fmt.Errorf("failed to parse document :%w", err)
	}

	app.OutputUsage(doc, option)

	return nil
}
