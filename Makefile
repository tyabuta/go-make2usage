binName=make2usage
cmdPackage=./cmd/make2usage

VERSION    = $(shell git describe --tags --abbrev=0 || echo "NO VERSION TAG")
REV        = $(shell git rev-parse --verify HEAD)
BUILT_AT   = $(shell date '+%Y-%m-%d %H:%M:%S %Z')
GO_VERSION = $(shell go version)

X_VERSION    = -X main.version=$(VERSION)
X_REV        = -X main.revision=$(REV)
X_BUILT_AT   = -X \"main.builtAt=$(BUILT_AT)\"
X_GO_VERSION = -X \"main.goVersion=$(GO_VERSION)\"

LDFLAGS = -ldflags "-w -s $(X_VERSION) $(X_REV) $(X_BUILT_AT) $(X_GO_VERSION)"


test:
	go test ./...

run-sample:
	go run $(cmdPackage)/main.go ./testdata/sample1.mk

run-help:
	go run $(cmdPackage)/main.go --help

build:
	go build $(LDFLAGS) -o bin/$(binName) $(cmdPackage)

build-mac:
	GOOS=darwin  GOARCH=amd64 go build $(LDFLAGS) -o bin/darwin64/$(binName)      $(cmdPackage)
build-linux64:
	GOOS=linux   GOARCH=amd64 go build $(LDFLAGS) -o bin/linux64/$(binName)       $(cmdPackage)
build-win64:
	GOOS=windows GOARCH=amd64 go build $(LDFLAGS) -o bin/windows64/$(binName).exe $(cmdPackage)
build-win32:
	GOOS=windows GOARCH=386   go build $(LDFLAGS) -o bin/windows32/$(binName).exe $(cmdPackage)

build-all:
	@make build-mac
	@make build-linux64
	@make build-win64
	@make build-win32

clean:
	rm -rf bin


usage:
	./bin/make2usage $(MAKEFILE_LIST) ./sample/*.mk

.PHONY: build clean
