make2usage
==========

## Easily install only a binary command.

Single binary file:make2usage download into the current directory.  
If you want to get a single binary simply, it just enough.

```bash
curl https://gitlab.com/tyabuta/go-make2usage/raw/master/install.sh | bash
```


## Use go command.

### go get

```bash
go get -v gitlab.com/tyabuta/go-make2usage
```

### go install

```bash
go get -v -u gitlab.com/tyabuta/go-make2usage/cmd/make2usage
```


## Usage

### make2usage --help

```
NAME:
   make2usage - Generate usage from Makefiles.

USAGE:
   make2usage [options] <Makefile, ...>

VERSION:
   0.1.0

AUTHOR:
   tyabuta <gmforip@gmail.com>

OPTIONS:
   --all-variable, -V  show all variable
   --all-command, -C   show all command
   --help, -h          show help
   --version, -v       print the version
```

## Output example

```
USAGE
    make <commands> [variables]

VARIABLES
    PWD       -- pwd
    CFLAGS    -- CFLAGS

COMMANDS
    task3     -- task3 desu
                 multi line
    task1     -- task1 desu
    test      -- Run tests
    deps      -- Install dependencies
    lint      -- Lint
    cover     -- Take coverage
    release   -- Release the binaries
    log-less  -- Show log with `less`
    log-tail  -- Show log with `tail`
    help      -- Show help
```

